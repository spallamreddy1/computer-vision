#include <iostream>
#include <stdexcept>
#include <opencv2/objdetect.hpp>
#include <iostream>
#include <stdexcept>
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videostab.hpp"
#include "opencv2/video.hpp"
#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/opencv_modules.hpp"
#include "stdafx.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

using namespace cv;
using namespace std;

Mat Pimg; Mat Ptempl; Mat Presult;
char* image_window = "Source Image";
//char* result_window = "Result window";

int match_method=5;
int max_Trackbar = 5;

const static Scalar colors[] =  { CV_RGB(0,0,255),CV_RGB(0,255,0),CV_RGB(255,0,0),CV_RGB(255,255,0),CV_RGB(255,0,255),
					CV_RGB(0,255,255),CV_RGB(255,255,255),CV_RGB(128,0,0),CV_RGB(0,128,0),CV_RGB(0,0,128),
					CV_RGB(128,128,128),CV_RGB(0,0,0)};

void MatchingMethod( int, void* );

class FPVitems     //main class
{
	public:	    
	
	Mat image_input;          //main input image
	Mat image_main_result;    //the final result
	Mat storage;              //introduced to stop detection of same car more than once

	CascadeClassifier cascade;    //the main cascade classifier
	CascadeClassifier checkcascade;  
	
	//a test classifier,car detected by both main and test is stated as car

	int num;

	void getimage(Mat src) //getting the input image
    {              
 
        if(! src.data )                             
        {
            cout <<  "src not filled" << endl ;
        }
 
        else
        {
            image_input = src.clone();
			storage = src.clone();              //initialising storage
			image_main_result = src.clone();    //initialising result
			
           // cout << "got image" <<endl;
        }
    }

	
	void cascade_load(string cascade_string)            //loading the main cascade
	{
		cascade.load(cascade_string);

		if( !cascade.load(cascade_string) )
    	{
        	cout << endl << "Could not load classifier cascade" << endl;
        	
    	}
		else
		{
			
		//	cout << "cascade : " << cascade_string << " loaded" << endl;
		}

	}


	void checkcascade_load(string checkcascade_string)               //loading the test/check cascade
	{
		checkcascade.load(checkcascade_string);

		if( !checkcascade.load(checkcascade_string) )
    	{
        	cout << endl << "Could not load classifier checkcascade" << endl;
        	
    	}
		else
		{
			//cout<< "checkcascade : " << checkcascade_string << " loaded" << endl;
		}
	}

	
	void display_input()             // function to display input
	{
		namedWindow("display_input");
		imshow("display_input",image_input);
		waitKey(0);
	}

	void display_output()            //function to display output
	{
		
		if(!image_main_result.empty() )
        {
			namedWindow("display_output");
			imshow("display_output",image_main_result);
			
		}
	}

	void setnum()                      
	{
		num = 0;
	}


	void findcars()                 //main function
	{
    	int i = 0;
    	
		Mat img = storage.clone();
		Mat temp;                    //for region of interest.If a car is detected(after testing) by one classifier,then it will not be available for other one
		
		if(img.empty() )
        {
			cout << endl << "detect not successful" << endl;
		}
		int cen_x;                         
		int cen_y;
    	vector<Rect> cars;
    	const static Scalar colors[] =  { CV_RGB(0,0,255),CV_RGB(0,255,0),CV_RGB(255,0,0),CV_RGB(255,255,0),CV_RGB(255,0,255),CV_RGB(0,255,255),CV_RGB(255,255,255),CV_RGB(128,0,0),CV_RGB(0,128,0),CV_RGB(0,0,128),CV_RGB(128,128,128),CV_RGB(0,0,0)};                   
        	
    	Mat gray; 

    	cvtColor( img, gray, CV_BGR2GRAY );

		Mat resize_image(cvRound (img.rows), cvRound(img.cols), CV_8UC1 );		

    	resize( gray, resize_image, resize_image.size(), 0, 0, INTER_LINEAR );
    	equalizeHist( resize_image, resize_image );

    	
    	cascade.detectMultiScale( resize_image, cars,1.1,2,0,Size(10,10));                 //detection using main classifier 
        	
		
		for( vector<Rect>::const_iterator main = cars.begin(); main != cars.end(); main++, i++ )
    	{
       		Mat resize_image_reg_of_interest;
        	vector<Rect> nestedcars;
        	Point center;
        	Scalar color = colors[i%8];
        	
	        	
			//getting points for bouding a rectangle over the car detected by main
			int x0 = cvRound(main->x);                              
			int y0 = cvRound(main->y);
			int x1 = cvRound((main->x + main->width-1));
			int y1 = cvRound((main->y + main->height-1));

		
			
        	if( checkcascade.empty() )
            	continue;
        	resize_image_reg_of_interest = resize_image(*main);
        	checkcascade.detectMultiScale( resize_image_reg_of_interest, nestedcars,1.1,2,0,Size(30,30));
            	
        	for( vector<Rect>::const_iterator sub = nestedcars.begin(); sub != nestedcars.end(); sub++ )      //testing the detected car by main using checkcascade
        	{
           		center.x = cvRound((main->x + sub->x + sub->width*0.5));        //getting center points for bouding a circle over the car detected by checkcascade
				cen_x = center.x;
			   	center.y = cvRound((main->y + sub->y + sub->height*0.5));
				cen_y = center.y;
				if(cen_x>(x0+15) && cen_x<(x1-15) && cen_y>(y0+15) && cen_y<(y1-15))         //if centre of bounding circle is inside the rectangle boundary over a threshold the the car is certified
				{
                								
					rectangle( image_main_result, cvPoint(x0,y0),
                    	   		cvPoint(x1,y1),
                     	  		color, 3, 8, 0);               //detecting boundary rectangle over the final result
					

					
					//masking the detected car to detect second car if present

					Rect region_of_interest = Rect(x0, y0, x1-x0, y1-y0);
					temp = storage(region_of_interest);
					temp = Scalar(255,255,255);

					num = num+1;     //num if number of cars detected
				
				}
			}	
				
		}		
				
                    	   
	if(image_main_result.empty() )
    {
		cout << endl << "result storage not successful" << endl;
	}			
			
    }      


	void findPedestrianSignal()                 
	{
    	int i = 0;
    	
		Mat img = storage.clone();
		//for region of interest.If a object is detected(after testing) by one classifier,then it will not be available for other one		

		Mat temp;                    
		
		if(img.empty() )
        {
			cout << endl << "detect not successful" << endl;
		}
		int cen_x;                         
		int cen_y;
    	vector<Rect> Objects;
    	const static Scalar colors[] =  { CV_RGB(255,0,255),CV_RGB(0,255,255),CV_RGB(255,255,255),CV_RGB(128,0,0),CV_RGB(0,128,0),CV_RGB(0,0,128),
					CV_RGB(128,128,128),CV_RGB(0,0,0)};                   
        	
    	Mat gray; 

    	cvtColor( img, gray, CV_BGR2GRAY );
		Mat resize_image;
		resize(gray,resize_image,Size(350,200),0,0,INTER_LINEAR); 
   	
    	cascade.detectMultiScale( resize_image , Objects,1.1,2,0,Size(10,10));                 //detection using classifier 
        	
		
	for( vector<Rect>::const_iterator main = Objects.begin(); main != Objects.end(); main++, i++ )
    	{
			cout << endl << "Pedestrian Signal Detected" << endl;
       		//Mat resize_image_reg_of_interest;
        	vector<Rect> nestedObjects;
        	Point center;
        	Scalar color = colors[i%8];
        	int radius;

	        	
			//getting points for bouding a rectangle over the object detected by main
			int x0 = cvRound(main->x);                              
			int y0 = cvRound(main->y);
			int x1 = cvRound((main->x + main->width-1));
			int y1 = cvRound((main->y + main->height-1));

		     								
				rectangle( image_main_result, cvPoint(x0,y0),
                    	   		cvPoint(x1,y1),
                     	  		color, 1, 8, 0);               //detecting boundary rectangle over the final result
					

					
					//masking the detected object to detect second object if present

					Rect region_of_interest = Rect(x0, y0, x1-x0, y1-y0);
					//temp = storage(region_of_interest);
					//temp = Scalar(255,255,255);		
		}		
				
                    	   
	if(image_main_result.empty() )
    {
		cout << endl << "result storage not successful" << endl;
	}			
			
    }      
			

	Mat pedestrianDetect(Mat &img)
{
    vector<Rect> found, found_filtered;
   
	HOGDescriptor hog;
    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
    hog.detectMultiScale(img, found, 0, Size(10,10), Size(32,32), 1.05, 2);
 // t = (double) getTickCount() - t;
    //cout << "detection time = " << (t*1000./cv::getTickFrequency()) << " ms" << endl; 

    for(size_t i = 0; i < found.size(); i++ )
    {
        Rect r = found[i];

        size_t j;
        // Do not add small detections inside a bigger detection.
        for ( j = 0; j < found.size(); j++ )
            if ( j != i && (r & found[j]) == r )
                break;

        if ( j == found.size() )
            found_filtered.push_back(r);

		cout << endl << "Pedestrian detected" << endl;
    }

    for (size_t i = 0; i < found_filtered.size(); i++)
    {
        Rect r = found_filtered[i];

        // The HOG detector returns slightly larger rectangles than the real objects,
        // so we slightly shrink the rectangles to get a nicer output.
        r.x += cvRound(r.width*0.1);
        r.width = cvRound(r.width*0.8);
        r.y += cvRound(r.height*0.07);
        r.height = cvRound(r.height*0.8);
        rectangle(img, r.tl(), r.br(), cv::Scalar(0,255,0), 3);
    }
	return img;
}
			
};

       		    	
int main( int argc, const char** argv )
{	
	             
    Mat image1 = imread("../../image/pedestrain.png",1);
	Mat image;
	
	//Creating an object
	FPVitems detectobj; 
	//GaussianBlur(image1,image,Size(7,7),0);
	image=detectobj.pedestrianDetect(image1);
	//imshow("people",image1);

	Mat frame;
	resize(image,frame,Size(350,200),0,0,INTER_LINEAR);        //resizing image to get best experimental results
	                     

	
	string checkcas = "../Cascade files/checkcas.xml";

	detectobj.getimage(frame);           //get the image
	detectobj.setnum();                  //set number of cars detected as 0
	detectobj.checkcascade_load(checkcas);      //load the test cascade
	
	string casFiles[]={"../Cascade files/cas1.xml","../Cascade files/cas2.xml","../Cascade files/cas3.xml","../Cascade files/cas4.xml"};
	//Applying various cascades for a finer search.

	if(casFiles)
	{
		for(int i = 0;i<4;i++)
		{
			string cas = casFiles[i];			
			detectobj.cascade_load(cas);            
			detectobj.findcars();
		}
	}
	else
	{
		//help();		
		cout << endl << "Please provide atleast one main cascade xml file" << endl;
	}
	

	detectobj.display_output();      //displaying the final result

	if(detectobj.num!=0)
	{
		cout << endl << "Number of cars detected: " << endl << detectobj.num;
	}

	string PedescasFiles[]={"../Cascade files/mainPcas_new.xml"};
	//Applying various cascades for a finer search.
	if(PedescasFiles)
	{
		for(int i = 0;i<1;i++)
		{
			string Pcas = PedescasFiles[i];			
			detectobj.cascade_load(Pcas);            
			detectobj.findPedestrianSignal();
		}
	}
	else
	{
			
		cout << endl << "Please provide atleast one main cascade xml file" << endl;
	}
	detectobj.display_output(); 
	waitKey(0);

	Pimg = imread( "../../image/pedestrain.png", 1 );
    Ptempl = imread( "../../image/8.jpg", 1 );
  
  
  /// Create windows
  namedWindow( image_window, CV_WINDOW_AUTOSIZE );
 // namedWindow( result_window, CV_WINDOW_AUTOSIZE );

  /// Create Trackbar
  char* trackbar_label = "Method: \n 0: SQDIFF \n 1: SQDIFF NORMED \n 2: TM CCORR \n 3: TM CCORR NORMED \n 4: TM COEFF \n 5: TM COEFF NORMED";
  createTrackbar( trackbar_label, image_window, &match_method, max_Trackbar, MatchingMethod );

  MatchingMethod( 0, 0 );

   cout << endl << "Signal is constant which is STOP" << endl;
   cout << endl << "NO GO for the Pedestrians" << endl;
   
  waitKey(0);
  return 0;
	
 
}

void MatchingMethod( int, void* )
{
  /// Source image to display
  Mat img_display;
  
  resize(Ptempl,Ptempl,Size(20,20),0,0,INTER_LINEAR);
  Pimg.copyTo( img_display );

  /// Create the result matrix
  int result_cols =  Pimg.cols - Ptempl.cols + 1;
  int result_rows = Pimg.rows - Ptempl.rows + 1;

  Presult.create( result_rows, result_cols, CV_32FC1 );

  /// Do the Matching and Normalize
  matchTemplate( Pimg, Ptempl, Presult, match_method );
  normalize( Presult, Presult, 0, 1, NORM_MINMAX, -1, Mat() );

  /// Localizing the best match with minMaxLoc
  double minVal; double maxVal; Point minLoc; Point maxLoc;
  Point matchLoc;

  minMaxLoc( Presult, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );

  /// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
  if( match_method  == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED )
    { matchLoc = minLoc; }
  else
    { matchLoc = maxLoc; }
  Scalar color = CV_RGB(0,0,255);

  /// Show me what you got
  rectangle( img_display, matchLoc, Point( matchLoc.x + Ptempl.cols , matchLoc.y + Ptempl.rows ), color, 2, 8, 0 );
  rectangle( Presult, matchLoc, Point( matchLoc.x + Ptempl.cols , matchLoc.y + Ptempl.rows ), color, 2, 8, 0 );

  imshow( image_window, img_display );
  //imshow( result_window, Presult );

  return;
}