Used Opencv 2.3.14
Visual Studio 2012 for C++

PreReq: opencv

This repository consists of 7 cascade classifier files and cpp files to use these cascade classifiers to detect Vehicles, Pedestrians and Pedestrian signal.

The image database for vehicles can be downloaded from here : http://cogcomp.cs.illinois.edu/Data/Car/

the image database for Pedestrian signal is provided in the repository.

The explanation of code is done with comments.

USAGE:

CASCADE TRAINING (haarTrainer)

1. Go to the directory of haarTrainer.exe file
2. Create two folders "neg" and "photo"   // "neg" folder should contain negative sample images and "photo" should contain positive samples
3. Open Command Prompt
4. Run the Following commands in order

harrTrainer.exe

harrTrainer.exe plist.txt photo\                     //This will open each image in the positive samples. Use "left mouse click" for marking, <space> for saving, <enter> for next image

opencv_createsamples.exe -info lent.txt -vec lentin.vec -w 25 -h 20          //creates vector file. create a negative.txt file with a list of negative image filenames in neg folder eg: nimage.jpg 

opencv_haartraining -data lent_out.xml -vec lentin.vec -bg neg\negative.txt -npos 60 -nneg 109 -nstages 15 -w 25 -h 20        // This will generate XML file

Note: "direct.h" can be downloaded from http://www.softagalleria.net/download/dirent/

FPVObjClassification :

Download all the files provided in "FPVObjClassification" folder (contains cascade files, cpp files)

Change the image path in the code if required

Provide the path of the template file.









